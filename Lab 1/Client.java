import java.net.*;
import java.io.*;

public class Client {
    
    public static void main(String args[]){
        Runnable r1 = new Receive();
        Thread receive = new Thread(r1);
        
        Runnable r2 = new Send();
        Thread send = new Thread(r2);
        
        send.start();
        receive.start();
        
    }
    
}

class Receive implements Runnable{
    public void run(){
        DatagramSocket socket = null;
        byte[] buffer = new byte[1000];
        try{
            socket = new DatagramSocket(6799);
            while(true){
                DatagramPacket request = new DatagramPacket(buffer,buffer.length);
                socket.receive(request);
            }
        }catch(SocketException e){
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }
    }
}


class Send implements Runnable{
    public void run(){
        DatagramSocket socket = null;
        byte[] buffer = new byte[1000];
        String message = null;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        
        try{
            
            
            
            socket = new DatagramSocket();
            while(true){
                message = reader.readLine();
                buffer = message.getBytes();
                
                DatagramPacket reply = new DatagramPacket(buffer,buffer.length,InetAddress.getByName("localhost"),6789);
                socket.send(reply);
            }
        }catch(SocketException e){
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }
    
    }
}